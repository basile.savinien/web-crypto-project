//import firebase from 'firebase/app'
//import 'firebase/auth'
import Vue from 'vue'

import jwt_decode from 'jwt-decode'
import { store } from '..'

export default {
  state: {
    currentUser:
      localStorage.getItem('user') != null
        ? JSON.parse(localStorage.getItem('user'))
        : null,
    loginError: null,
    signupStatus: null,
    processing: false
  },
  getters: {
    currentUser: state => state.currentUser,
    processing: state => state.processing,
    loginError: state => state.loginError,
    signupStatus: state => state.signupStatus
  },
  mutations: {
    setUser(state, payload) {
      state.currentUser = payload
      state.processing = false
      state.loginError = null
    },
    setLogout(state) {
      state.currentUser = null
      state.processing = false
      state.loginError = null
    },
    setProcessing(state, payload) {
      state.processing = payload
      state.loginError = null
    },
    setError(state, payload) {
      state.loginError = payload
      state.signupStatus = payload
      state.currentUser = null
      state.processing = false
    },
    setSignupStatus(state, payload) {
      state.signupStatus = payload
      state.processing = false
    },
    clearError(state) {
      state.loginError = null
    }
  },
  actions: {
    login({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)
      Vue.prototype.$http
        .post('http://localhost:3000/api/users/login', {
          username: payload.username,
          password: payload.password
        })
        .then(
          res => {
            // console.log(res, 'tag res')
            const token = res.data.token
            localStorage.setItem('token', token)
            // eslint-disable-next-line standard/computed-property-even-spacing
            Vue.prototype.$http.defaults.headers.common[
              'Authorization'
            ] = `Bearer ${token}`
            var decoded = jwt_decode(token)

            if (decoded) {
              // @ts-ignore
              store.dispatch('getUser', decoded.id).then(res => {
                // console.log(res, 'tag getUser')
                // @ts-ignore
                const item = { uid: decoded.id, ...res.data }
                localStorage.setItem('user', JSON.stringify(item))
                commit('setUser', { uid: item.uid, ...res.data })
              })
            }
          },
          err => {
            localStorage.removeItem('user')
            commit('setError', err.message)
          }
        )
    },
    async facebook({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)

      const token = payload
      localStorage.setItem('token', token)
      Vue.prototype.$http.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${token}`
      // console.log("token", token)
      var decoded = jwt_decode(token)

      if (decoded) {
        // @ts-ignore
        await store.dispatch('getUser', decoded.id).then(res => {
          // console.log(res, 'tag getUser')
          // @ts-ignore
          const item = { uid: decoded.id, ...res.data }
          localStorage.setItem('user', JSON.stringify(item))
          commit('setUser', { uid: item.uid, ...res.data })
        })
      }
    },
    signIn({ commit }, payload) {
      // console.log('in register', payload)
      commit('clearError')
      commit('setProcessing', true)
      Vue.prototype.$http
        .post('http://localhost:3000/api/users/register', {
          username: payload.username,
          password: payload.password,
          email: payload.email,
          firstname: payload.firstname,
          lastname: payload.lastname,
          birthday: new Date()
        })
        .then(
          res => {
            // console.log(res, 'tag res')
            commit('setSignupStatus', 'Inscription réussi')
          },
          err => {
            localStorage.removeItem('user')
            commit('setSignupStatus', err.message)
          }
        )
    },
    getUser({ commit }, id) {
      return Vue.prototype.$http.get('http://localhost:3000/api/users/' + id)
    },
    signOut({ commit }) {
      localStorage.removeItem('user')
      localStorage.removeItem('token')
      // window.location.reload()
      commit('setLogout')
    }
  }
}
