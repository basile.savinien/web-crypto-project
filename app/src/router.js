import Vue from 'vue'
import Router from 'vue-router'
import AuthRequired from '@/utils/AuthRequired'

Vue.use(Router)

const routes = [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "app" */ './views/app'),
    redirect: '/app/home',
    // beforeEnter: AuthRequired,
    children: [
      {
        path: 'app/home',
        component: () =>
          import(/* webpackChunkName: "piaf" */ './views/app/home'),
        redirect: '/app/cryptos/all',
        children: [
          {
            path: 'start',
            component: () =>
              import(/* webpackChunkName: "piaf" */ './views/app/home/Start')
          }
        ]
      },
      {
        path: 'app/second-menu',
        component: () =>
          import(
            /* webpackChunkName: "second-menu" */ './views/app/secondMenu'
          ),
        redirect: '/app/second-menu/second',
        children: [
          {
            path: 'second',
            component: () =>
              import(
                /* webpackChunkName: "second-menu" */ './views/app/secondMenu/Second'
              )
          }
        ]
      },
      {
        path: 'app/single',
        component: () =>
          import(/* webpackChunkName: "single" */ './views/app/single')
      },
      {
        path: 'app/profile',
        component: () =>
          import(/* webpackChunkName: "single" */ './views/user/Profile')
      },
      {
        path: 'app/user/list',
        component: () =>
          import(/* webpackChunkName: "user" */ './views/user/List')
      },
      {
        path: 'app/cryptos/all',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/crypto/List')
      },
      {
        path: 'app/cryptos/my',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/crypto/My_list.vue')
      },
      {
        path: 'app/cryptos/history',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/crypto/History.vue')
      },
      {
        path: 'app/articles/all',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/article/List')
      },
      {
        path: 'app/articles/my',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/article/My_list')
      },
      {
        path: 'app/preferences',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/user/Preference')
      },
      {
        path: 'app/rss/list',
        component: () =>
          import(/* webpackChunkName: "error" */ './views/rss/List')
      }
    ]
  },
  {
    path: '/error',
    component: () => import(/* webpackChunkName: "error" */ './views/Error')
  },
  {
    path: '/facebookSuccess',
    component: () =>
      import(/* webpackChunkName: "error" */ './views/SuccessFacebook')
  },
  {
    path: '/user',
    component: () => import(/* webpackChunkName: "user" */ './views/user'),
    redirect: '/user/login',
    children: [
      {
        path: 'login',
        component: () =>
          import(/* webpackChunkName: "user" */ './views/user/Login')
      },
      {
        path: 'register',
        component: () =>
          import(/* webpackChunkName: "user" */ './views/user/Register')
      },
      {
        path: 'forgot-password',
        component: () =>
          import(/* webpackChunkName: "user" */ './views/user/ForgotPassword')
      }
    ]
  },
  // {
  //   path: '/article',
  //   component: () => import(/* webpackChunkName: "error" */ './views/Article')
  // },
  {
    path: '*',
    component: () => import(/* webpackChunkName: "error" */ './views/Error')
  }
]

const router = new Router({
  linkActiveClass: 'active',
  routes,
  mode: 'history'
})

export default router
