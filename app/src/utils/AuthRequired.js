export default (to, from, next) => {
  if (
    localStorage.getItem('user') != null &&
    localStorage.getItem('token') != null
  ) {
    next()
  } else {
    localStorage.removeItem('user')
    next('/user/login')
  }
}
