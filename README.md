
# T-WEB-700

## Requirements

- Nodejs 14.x
- PostgreSQL 8.4
- Docker

## Docker Installation

- Run `docker-compose up --build` to push all the folder and modification to the Docker containers and run it at the same time

If it's the first execution and the DB is empty
- Run `docker ps` to find the container ID of the back container
- Run `docker exec -it CONTAINER_ID /bin/bash` to enter the back container
- Run `npx sequelize db:seed:all --env="prod"` to seed the data in the DB
- Now you may wait for the back end to update every data dependings on APIs (2 min to 30 min), or we advise you stop the docker-compose and run again `docker-compose up` to instantly update


## Local Installation

### Front :
- Run `cd app`
- Run `npm install` to download the dependencies
- Run `npm run serve` to launch the VueJS server

### back :
- Run `cd back`
- Run `npm install` to download the dependencies

If your DB was not created yet :
- Run `npx createdb -U postgres t_web_700` to create the database
- Run `npx sequelize db:migrate --env="dev"` to create or update table from migrations
- Run `npm run dev` to start the server locally and create db associations

If your DB is still empty :
- Stop the server and push the migration with : `npx sequelize db:seed:all --env="dev"` to import default data in tables
- Run `npm run dev` to launch the NodeJS back end locally <br>
<br>

- Run `npm run dev` to start the server locally and create db associations

## Informations

- To create a table :
  `npx sequelize model:generate --name User --attributes username:string,lastname:string,email:string`
- To create fake data in table :
  `npx sequelize seed:generate --name User`
- To remove all tables : `npm run removedb` 
 or like that : `npx sequelize db:migrate:undo:all --env="dev"`
