# Timemanager API

## Requirements

- Nodejs 14.x
- PostgreSQL 8.4

## Installation

- Run `npm install` to download the dependencies
- Run PostgreSql server
- Run `npx createdb -U postgres t_web_700` to create the database
- Run `npm start` to start the server and create db associations
- Run `npm run createdb` migrate & seed <br>
or manually :
- Run `npx sequelize db:migrate --env="dev"` to create or update table from migrations
- Run `npx sequelize db:seed:all --env="dev"` to import default data in tables

## Informations

- To create a table :
  `npx sequelize model:generate --name User --attributes username:string,lastname:string,email:string`
- To create fake data in table :
  `npx sequelize seed:generate --name User`
- To remove all tables : `npm run removedb` <br>
 or like that : `npx sequelize db:migrate:undo:all --env="dev"`
