'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Rsses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      link: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING
      },
      site: {
        type: Sequelize.STRING
      },
      interval: {
        type: Sequelize.INTEGER,
        defaultValue: 15 //15 minutes
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addColumn(
      'Articles', // name of Source model
      'rssId', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Rsses', // name of Target model
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },
  down: async (queryInterface, Sequelize) => { 
    await queryInterface.dropTable('Articles');
    await queryInterface.dropTable('Rsses');
    await queryInterface.removeColumn(
      'Articles',
      'RssId'
    )
  }
};
