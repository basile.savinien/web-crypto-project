'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Cryptos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      id_api: {
        type: Sequelize.STRING
      },
      current_price_usd: {
        type: Sequelize.FLOAT
      },
      current_price_eur: {
        type: Sequelize.FLOAT
      },
      price_change_percentage_24h: {
        type: Sequelize.FLOAT
      },
      lowest_price_eur: {
        type: Sequelize.FLOAT
      },
      lowest_price_usd: {
        type: Sequelize.FLOAT
      },
      highest_price_eur: {
        type: Sequelize.FLOAT
      },
      highest_price_usd: {
        type: Sequelize.FLOAT
      },
      url_image: {
        type: Sequelize.STRING
      },
      evolution_price: {
        type: Sequelize.STRING
      },
      is_active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserCryptos');
    await queryInterface.dropTable('Cryptos');
  }
};
