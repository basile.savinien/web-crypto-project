module.exports = {
  prod: {
    url: 'postgres://postgres:postgres@db:5432/t_web_700',
    dialect: 'postgres',
  },
  dev: {
    username: 'postgres',
    password: 'postgres',
    database: 't_web_700',
    host: 'localhost',
    dialect: 'postgres',
  },
};
