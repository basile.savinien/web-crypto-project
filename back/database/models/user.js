'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcryptjs");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsToMany(models.Crypto, {
        through: 'UserCrypto',
        as: 'cryptos',
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        type: DataTypes.BIGINT,
      });
    }
  };
  User.init({
    id: {
      primaryKey: true,
      type: DataTypes.BIGINT,
      autoIncrement: true
    },
    username: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    birthday: DataTypes.DATE,
    nickname: DataTypes.STRING,
    currency: DataTypes.STRING,
    oauthId: {
      type: DataTypes.STRING,
      unique: true
    },
    role: {
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    password: DataTypes.STRING,
    keywords: {
      type: DataTypes.STRING,
      defaultValue: ";",
      get() {
          if(this.getDataValue('keywords')){
            return this.getDataValue('keywords').split(';').filter(e => e!="");
          }
        },
      set(val) {
        this.setDataValue('keywords',val.join(';'));
      },
    },
  }, {
    sequelize,
    modelName: 'User',
  });

  User.beforeCreate((user, options) => {
    const userPassword = user.getDataValue("password");
    if (userPassword) {
      return bcrypt.hash(userPassword, 10)
          .then(hash => {
            user.setDataValue("password", hash);
          })
          .catch(err => {
              throw new Error();
          });
    }
  });

  return User;
};
