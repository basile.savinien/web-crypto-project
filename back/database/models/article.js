'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Article.belongsTo(models.Rss, {
        foreignKey: 'id',
        as: 'rss',
        onDelete: 'CASCADE',
      });
    }
  }
  Article.init(
    {
      title: DataTypes.STRING,
      author: DataTypes.STRING,
      link: {
        type: DataTypes.STRING,
        unique: true
      },
      description: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'Article',
    }
  );

  return Article;
};
