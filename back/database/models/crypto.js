'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Crypto extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Crypto.belongsToMany(models.User, {
        through: 'UserCrypto',
        as: 'users',
        foreignKey: 'cryptoId',
        onDelete: 'CASCADE',
      });
    }
  }
  Crypto.init(
    {
      name: DataTypes.STRING,
      id_api: DataTypes.STRING,
      current_price_usd: DataTypes.FLOAT,
      current_price_eur: DataTypes.FLOAT,
      price_change_percentage_24h: DataTypes.FLOAT,
      lowest_price_eur: DataTypes.FLOAT,
      lowest_price_usd: DataTypes.FLOAT,
      highest_price_eur: DataTypes.FLOAT,
      highest_price_usd: DataTypes.FLOAT,
      evolution_price: DataTypes.STRING,
      url_image: DataTypes.STRING,
      is_active: {
        defaultValue: false,
        type: DataTypes.BOOLEAN,
      },
    },
    {
      sequelize,
      modelName: 'Crypto',
    }
  );

  return Crypto;
};
