'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rss extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Rss.hasMany(models.Article, {
        foreignKey: 'rssId',
        as: 'articles',
        onDelete: 'CASCADE',
      });
    }
  }
  Rss.init(
    {
      link: DataTypes.STRING,
      site: DataTypes.STRING,
      interval : DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'Rss',
    }
  );

  return Rss;
};
