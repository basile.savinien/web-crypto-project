'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Rsses',
      [
        {
          link: 'https://feeds.feedburner.com/coinspeaker/',
          site: 'Coin Speaker',
          interval : 15,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          link: 'https://www.coindesk.com/feed',
          site: 'Coin Desk',
          interval : 5,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          link: 'https://cryptopotato.com/feed',
          site: 'Cypto Potato',
          interval : 30,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          link: 'https://bitcoinmagazine.com/feed',
          site: 'BitCoin Magazine',
          interval : 60,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Rsses', null, {});
  },
};
