'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'UserCryptos',
      [
        {
          amount: 10.5,
          userId: 1,
          cryptoId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          amount: 10.5,
          userId: 2,
          cryptoId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          amount: 11.5,
          userId: 1,
          cryptoId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          amount: 11.5,
          userId: 2,
          cryptoId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserCryptos', null, {});
  },
};
