'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Users',
      [
        {
          username: 'admin',
          password:
            '$2a$06$IMYYTt4GQOtcxdJyACik1.qskLjYU0GQRcYpGdYfqxEIDqugb4Nry',
          firstName: 'admin',
          lastName: 'admin',
          email: 'admin@admin.com',
          birthday: '2020-01-01',
          currency: 'EUR',
          createdAt: new Date(),
          updatedAt: new Date(),
          role: true,
          keywords: "Bitcoin;Cash;",
        },
        {
          username: 'Rompiot',
          password:
            '$2a$06$IMYYTt4GQOtcxdJyACik1.qskLjYU0GQRcYpGdYfqxEIDqugb4Nry',
          firstName: 'Romain',
          lastName: 'PIOT',
          email: 'romainpiot.pro@gmail.com',
          birthday: '1991-12-10',
          currency: 'EUR',
          createdAt: new Date(),
          updatedAt: new Date(),
          role: false,

        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  },
};
