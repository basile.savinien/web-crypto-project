'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Articles',
      [
        {
          title: 'test 1',
          author: 'test 1',
          link: 'test2',
          rssId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          title: 'test 1',
          author: 'test 1',
          link: 'test3',
          rssId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Articles', null, {});
  },
};
