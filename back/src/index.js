const dotenv = require('dotenv/config');
var express = require('express');
const coockieParser = require('cookie-parser');
const cors = require('cors');
const passport = require('passport');
const passportJwt = require('passport-jwt');
const { jwtOptions } = require('./utils/jwt');
const { User } = require('../database/models');
const json = require('body-parser');

const rssSubscriber = require('./controllers/rssSubscriber.js');
const cryptoSubscriber = require('./subscribers/cryptoSubscriber');

let JwtStrategy = passportJwt.Strategy;

const optionsJwt = jwtOptions();
// lets create our strategy for web token
let strategy = new JwtStrategy(optionsJwt, function (jwt_payload, next) {
  // console.log('payload received', jwt_payload);
  User.findAll({ where: { id: jwt_payload.id } }).then((user) => {
    if (user) {
      next(null, user);
    } else {
      next(null, false);
    }
  });
});

// use the strategy
passport.use(strategy);
const session = require('express-session')
const usersRoutes = require('./routes/users.js');
const authRoutes = require('./routes/oauth.js');
const cryptosRoutes = require('./routes/cryptos.js');
const articlesRoutes = require('./routes/articles.js');
const rssesRoutes = require('./routes/rss.js');
const userCryptosRoutes = require('./routes/userCryptos.js');

const hostname = '127.0.0.1';
const port = 3000;
const app = express();
app.use(session({ secret: 'anything' }));
app.use(passport.initialize());
app.use(passport.session());


app.use(
  cors({
    origin: true,
    credentials: true,
  })
);

app.use(coockieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api/users', usersRoutes);
app.use('/api/cryptos', cryptosRoutes);
app.use('/api/articles', articlesRoutes);
app.use('/api/myCryptos', userCryptosRoutes);
app.use('/api/rss', rssesRoutes);
app.use(passport.initialize());

app.use(json());
app.use("/", authRoutes);

// test protected route
app.get(
  '/api/protected',
  passport.authenticate('jwt', { session: false }),
  function (req, res) {
    res.json({
      msg: 'You are authorized',
    });
  }
);

// console.log(process.env.MY_SECRET);

app.listen(port, function () {
  console.log(`Example app listening on port ${hostname}:${port} !`);
});
