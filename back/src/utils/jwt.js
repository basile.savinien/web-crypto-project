const jwt = require('jsonwebtoken');
const passport = require('passport');
const passportJwt = require('passport-jwt');
require('dotenv').config()

exports.jwtOptions = function () {
  let ExtractJwt = passportJwt.ExtractJwt;

  let jwtOptions = {};
  jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  jwtOptions.secretOrKey = process.env.SECRET_KEY;
  return jwtOptions;
};

exports.checkJwt = function () {
  const pass = passport.authenticate('jwt', { session: false });
  return pass;
}