const { User, Crypto, UserCrypto } = require('../../database/models');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/jwt');
const userController = require('./userController');
const cryptoController = require('./cryptoController');
const { Op } = require('sequelize');

exports.sub = function (req, res) {
    const userId = req.params.user_id
    const cryptoId = req.params.cryptos_id
    // console.log("user : ", userId, "crypto : ", cryptoId)
    // const roleLevel = 1;
    // check empty field
    if (userId === null || cryptoId === null) {
        res.status(400).json({ error: 'missing fields' });
    }

    UserCrypto.findOne({
        attributes: ['userId', 'cryptoId'],
        where: {
            [Op.or]: [{ cryptoId: cryptoId, userId: userId }],
        },
    }).then((cryptoExist) => {
        if (!cryptoExist) {
            UserCrypto.create({ cryptoId: cryptoId, userId: userId }).then((crypto) => {
                res.status(201).json({ crypto });
            });
        } else {
            res.status(409).json({ error: 'Crypto already exist' });
        }
    });
};

exports.unSub = function (req, res) {
    const userId = req.params.user_id
    const cryptoId = req.params.cryptos_id
    // console.log("user : ", userId, "crypto : ", cryptoId)
    // const roleLevel = 1;
    // check empty field
    if (userId === null || cryptoId === null) {
        res.status(400).json({ error: 'missing fields' });
    }

    UserCrypto.destroy({
        where: {
            userId: userId,
            cryptoId: cryptoId
        },
    }).then((usercrypto) => {
        res.json({ message: 'UserCryptos deleted' });
    });
};

exports.getAll = async function (req, res) {
    userController.getUserConnected(req).then((user)=>{
    User.findOne({
        where: {
            id: user.id,
        },
        include: [{
          model: Crypto,
          as: 'cryptos'
        }]

    }).then((usercrypto) => {
        res.json(usercrypto.cryptos);
    });
  });
};
