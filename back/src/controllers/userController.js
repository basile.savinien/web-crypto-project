const { User } = require('../../database/models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/jwt');
const userController = require('../controllers/userController');
const { Op } = require('sequelize');
const passport = require('passport');
const dotenv = require('dotenv/config');
const strategy = require('passport-facebook');

// Get one user by user_id
exports.getById = function (req, res) {
  const { user_id } = req.params;
  // const { roleLevel, userId } = userController.getUserConnected(req, res);
  const roleLevel = 1;
  const userId = user_id;

  if (roleLevel == 1 || roleLevel == 2) {
    User.findByPk(user_id, {}).then((user) => {
      res.json(user);
    });
  } else {
    User.findOne({
      where: {
        id: userId,
      },
    }).then((user) => {
      res.json(user);
    });
  }
};

// Authenticate user
exports.login = function (req, res) {
  const username = req.body.username;
  const password = req.body.password;

  // check empty field
  if (username == null || password == null) {
    res.status(400).json({ error: 'missing fields' });
  }

  User.findOne({
    where: {
      username: username,
    },
  })
    .then((userFound) => {
      if (userFound) {
        bcrypt.compare(password, userFound.password, (errBcrypt, resBcrypt) => {
          if (resBcrypt) {
            let payload = { id: userFound.id };
            let optionsJwt = jwtOptions();
            let token = jwt.sign(payload, optionsJwt.secretOrKey);
            res.json({ message: 'user connected', token: token });
          } else {
            res.status(500).json({ error: 'Password error' });
          }
        });
      } else {
        res.status(500).json({ error: 'Fail connection' });
      }
    })
    .catch((err) => {
      res.status(500).json(err);
    });
};

// create user
exports.create = function (req, res) {
  console.log('req', req.body);
  console.log('res', res);
  const username = req.body.username;
  const password = req.body.password;
  const email = req.body.email;
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const birthday = req.body.birthday;

  // check empty field
  if (username == null || email == null || password == null) {
    res.status(400).json({ error: 'missing fields' });
  }

  User.findOne({
    attributes: ['email', 'username'],
    where: {
      [Op.or]: [{ email: email }, { username: username }],
    },
  }).then((userExist) => {
    if (!userExist) {
      User.create({
        username: username,
        password: password,
        email: email,
        firstName: firstname,
        lastName: lastname,
        birthday: birthday,
      }).then((user) => {
        console.log('test');
        res.status(201).json({ user });
      });
    } else {
      res.status(409).json({ error: 'User already exist in db' });
    }
  });
};

// Log out user
exports.logout = function (req, res) {
  res.json({ message: 'User disconnected' });
};

// Delete a user
exports.delete = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'user', res);
  const userRole = await userController.getRole(jwtToken);
  const { user_id } = req.params;

  // TODO : check if admin OR userId == userconnected
  if ((userRole == "admin") || (user_id == user_id)) {
    User.destroy({
      where: {
        id: user_id,
      },
    }).then((user) => {
      res.json({ message: 'user deleted' });
    });
  } else {
    res.json({ message: "you don't have the rights to do this" });
  }
};

exports.update = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'user', res);
  const userRole = await userController.getRole(jwtToken);
  const { user_id } = req.params;
  const userValues = req.body;

  // TODO : check if admin OR userId == userconnected
  if ((userRole == "admin") || (user_id == user_id)) {
    User.update(userValues, {
      where: {
        id: user_id,
      },
    }).then((user) => {
      res.json(user);
    });
  } else {
    res.json({ message: "you don't have the rights to do this" });
  }
};

// Get all users
exports.getAll = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'user', res);

  const username = req.query.username;
  const email = req.query.email;

  if (username) {
    User.findOne({
      where: {
        username: username,
      },
    }).then((user) => {
      res.json(user);
    });
  } else if (email) {
    User.findOne({
      where: {
        email: email,
      },
    }).then((user) => {
      res.json(user);
    });
  } else {
    // return all users
    User.findAll().then((users) => {
      res.json(users);
    });
  }
};

exports.getUserConnected = async function (req) {
  const jwtToken = req.header('authorization').split(' ')[1];
  return userController.getUserFromToken(jwtToken);
};

exports.checkAccessFromReq = async function (req , roleNeeded , res ) {
  const jwtToken = req.header('authorization').split(' ')[1];
  return userController.checkAccess(jwtToken,roleNeeded,res );
};


exports.checkAccess = async function (jwtToken, roleNeeded, res) {
  const userRole = await userController.getRole(jwtToken);
  let levelUser = 0;

  if (userRole == 'admin') {
    levelUser = 10;
  } else if (userRole == 'user') {
    levelUser = 5;
  } else {
    levelUser = 0;
  }

  if (roleNeeded == 'admin') {
    if (levelUser < 10) {
      res.json({ message: 'You are not authorized' });
    }
  } else if (roleNeeded == 'user') {
    if (levelUser < 5) {
      res.json({ message: 'You are not authorized' });
    }
  }
};

exports.getRole = async function (jwtToken) {
  const currentUser = userController.decryptToken(jwtToken);
  let role = '';

  // @ts-ignore
  await User.findByPk(currentUser.id).then((userFound) => {
    if (!userFound) {
      role = 'disconnected';
    } else if (userFound.dataValues.role == false) {
      role = 'user';
    } else if (userFound.dataValues.role == true) {
      role = 'admin';
    } else {
      role = 'disconnected';
    }
  });

  return role;
};

exports.getUserFromToken = async function (jwtToken) {
  const currentUser = userController.decryptToken(jwtToken);

  return User.findByPk(currentUser.id);
};

exports.decryptToken = function (jwtToken) {
  const optionsJwt = jwtOptions();
  const secretKey = optionsJwt.secretOrKey;
  let returned;

  if (!jwtToken) {
    returned = 'error';
  }

  //check jwt
  jwt.verify(jwtToken, secretKey, function (err, jwtValues) {
    if (jwtValues) {
      returned = jwtValues;
    } else {
      returned = 'error';
    }
  });
  return returned;
};

const express = require('express');

const FacebookStrategy = strategy.Strategy;
const router = express();

passport.serializeUser(function (user, done) {
  console.log('user serialize', user.id);
  done(null, user.id);
});

passport.deserializeUser(function (obj, done) {
  console.log('obj', obj);
  done(null, obj);
});

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      callbackURL: process.env.FACEBOOK_CALLBACK_URL,
      profileFields: ['id', 'emails', 'name'],
    },
    async function (accessToken, refreshToken, profile, done) {
      console.log('profile', profile);
      // console.log(profile.emails[0].value, profile.name.givenName, profile.name.familyName)
      // console.log(emails, first_name, last_name)
      const userData = {
        id: profile.id,
        username: profile.name.givenName,
        email: profile.emails[0].value,
        firstName: profile.name.givenName,
        lastName: profile.name.familyName,
        oauthId: profile.id,
      };
      console.log('userData', userData);
      const email = profile.emails[0].value;

      await User.findOne({
        attributes: ['email'],
        where: {
          [Op.or]: [{ email: email }],
        },
      }).then((userExist) => {
        // let payload = { id: userExist.id};
        if (!userExist) {
          User.create(userData).then((user) => {
            console.log('user', user);
            done(null, user);
          });
        } else {
          console.log('exists', profile);
          done(null, profile);
          // done(null, profile);
        }
      });
    }
  )
);
