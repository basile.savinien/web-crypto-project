const { User, Crypto } = require('../../database/models');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/jwt');
const userController = require('./userController');
const cryptoController = require('./cryptoController');
const { Op } = require('sequelize');
const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();

// Get one user by user_id
exports.getById = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'user', res);

  const { crypto_id } = req.params;

  Crypto.findByPk(crypto_id, {}).then((crypto) => {
    res.json(crypto);
  });
};

// Get one user by user_id
exports.getHistoryById = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'user', res);
  const { crypto_api_id, period } = req.params;
  const { currency } = req.body;

  // daily: Last 60 days, so 60 periods a day
  // hourly: 48 last hours, so 48 periods of one hour
  // minute: last 2 hours, so 60 periods of one minute

  let cryptoData;

  const add_minutes = function (dt, minutes) {
    return Math.floor(new Date(dt.getTime() - minutes * 60000).getTime() / 1000);
  };

  const date = new Date();
  const now = Math.floor(date.getTime() / 1000);
  const last2Hours = add_minutes(date, 60 * 2);
  const last48Hours = add_minutes(date, 60 * 48);
  const last60days = add_minutes(date, 60 * 24 * 60);

  if (period == 'daily') {
    cryptoData = await CoinGeckoClient.coins.fetchMarketChart(crypto_api_id, {
      vs_currency: currency,
      // @ts-ignore
      days: 60,
      interval: 'daily',
    });
  } else if (period == 'hourly') {
    cryptoData = await CoinGeckoClient.coins.fetchMarketChart(crypto_api_id, {
      vs_currency: currency,
      // @ts-ignore
      days: 2,
    });
  } else if (period == 'minute') {
    cryptoData = await CoinGeckoClient.coins.fetchMarketChartRange(crypto_api_id, {
      vs_currency: currency,
      from: last2Hours,
      to: now,
    });
  }

  res.json(cryptoData.data);
};

// create user
exports.create = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'admin', res);

  const { name, id_api, current_price_usd, current_price_eur, is_active } = req.body;

  // check empty field
  if (name == null) {
    res.status(400).json({ error: 'missing fields' });
  }

  Crypto.findOne({
    attributes: ['name'],
    where: {
      [Op.or]: [{ name: name }],
    },
  }).then((cryptoExist) => {
    if (!cryptoExist) {
      Crypto.create({
        name: name,
        id_api: id_api,
        current_price_usd, current_price_usd,
        current_price_eur: current_price_eur,
        is_active, is_active
      }).then((crypto) => {
        res.status(201).json({ crypto });
      });
    } else {
      res.status(409).json({ error: 'Crypto already exist' });
    }
  });
};

// Delete a crypto
exports.delete = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'admin', res);

  const { crypto_id } = req.params;

  Crypto.destroy({
    where: {
      id: crypto_id,
    },
  }).then((crypto) => {
    res.json({ message: 'crypto deleted' });
  });
};

exports.update = async function (req, res) {
  const jwtToken = req.header('authorization').split(' ')[1];
  await userController.checkAccess(jwtToken, 'admin', res);

  const { crypto_id } = req.params;
  const cryptoValues = req.body;
  console.log(cryptoValues);

  Crypto.update(cryptoValues, {
    where: {
      id: crypto_id,
    },
  }).then((crypto) => {
    res.json({ message: 'Crypto updated' });
  });
};

// Get all cryptos
exports.getAll = async function (req, res) {
  let curUser;

  if (req.header('authorization')) {
    curUser = await userController.getUserConnected(req);
  } else {
    curUser = null;
  }

  if (curUser == null) {
    Crypto.findAll({
      include: [
        {
          model: User,
          as: 'users',
        },
      ],
      where: {
        is_active: true,
      },
    }).then((cryptos) => {
      res.json(cryptos);
    });
  } else if (curUser.role == true) {
    Crypto.findAll({}).then((cryptos) => {
      res.json(cryptos);
    });
  } else {
    Crypto.findAll({
      include: [
        {
          model: User,
          as: 'users',
        },
      ],
      where: {
        is_active: true,
      },
    }).then((cryptos) => {
      cryptos.forEach((crypto) => {
        var subs = false;
        var id = 0;
        while (!subs && crypto.users[id] != null) {
          if (crypto.users[id].id === curUser.id) {
            subs = true;
            console.log(crypto);
          }
          id++;
        }
        if (subs) {
          crypto.dataValues.subscribed = 'true';
        } else {
          crypto.dataValues.subscribed = 'false';
        }
      });

      res.json(cryptos);
    });
  }
};

exports.getActive = async function (req, res) {
  Crypto.findAll({}).then((cryptos) => {
    console.log('test', cryptos);
    res.json(cryptos);
  });
};

exports.getCryptosDataFromApi = async function (req, res) {
  let activeCryptos = await Crypto.findAll({
    attributes: ['id_api'],
    where: {
      is_active: true,
    },
  });
  activeCryptos = activeCryptos.map((crypto) => crypto.id_api);

  const list = [];

  list['eur'] = await cryptoController.getCryptoData(activeCryptos, 'eur');
  list['usd'] = await cryptoController.getCryptoData(activeCryptos, 'usd');

  // res.json(list['eur']);
  return list;
};

exports.getCryptoData = async function (activeCryptos, currency) {
  // @ts-ignore
  const cryptoData = await CoinGeckoClient.coins.markets({
    ids: activeCryptos,
    vs_currency: currency,
  });

  return cryptoData.data;
};

exports.getCryptoList = async function (req, res) {
  const cryptoList = await CoinGeckoClient.coins.list();

  let cryptoListArray = cryptoList.data;
  cryptoListArray = cryptoListArray.map((crypto) => crypto.id);

  res.json(cryptoListArray);
};
