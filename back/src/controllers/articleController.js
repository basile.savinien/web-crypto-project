const { User, Article } = require('../../database/models');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/jwt');
const userController = require('./userController');
const { Op } = require('sequelize');


// Get one article by article_id
exports.getById = function (req, res) {
  const { article_id } = req.params;

  Article.findByPk(article_id, {}).then((article) => {
    res.json(article);
  });
};

// Get all article of connected user
exports.getMyArticles = async function (req, res) {

  userController.getUserConnected(req).then((user)=>{
    const keywords = user.keywords.map(keyword => `%${keyword}%`);

    Article.findAll({
        where:{
          title:{
          [Op.iLike]:{ [Op.any]: keywords }
          }
        },
        order: [
                ['id', 'DESC'],
        ],

    }).then((articles) => {
      res.json(articles);
    });
  });


};


// Delete a article
exports.delete = function (req, res) {
  const { article_id } = req.params;

  const roleLevel = 1;
  if (roleLevel == 1) {
    Article.destroy({
      where: {
        id: article_id,
      },
    }).then((article) => {
      res.json({ message: 'Article deleted' });
    });
  } else {
    res.json({ message: "you don't have the rights to do this" });
  }
};

//Update article
exports.update = function (req, res) {
  const { article_id } = req.params;
  const articleValues = req.body;
  console.log(articleValues);

  Article.update(articleValues, {
    where: {
      id: article_id,
    },
  }).then((article) => {
    res.json({ message: 'Article updated' });
  });
};

// Get all Articles
exports.getAll = async function (req, res) {
  Article.findAll({
    order: [
            ['id', 'DESC'],
    ],
  }).then((articles) => {
    res.json(articles);
  });
};
