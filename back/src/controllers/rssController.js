const { User, Rss } = require('../../database/models');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/jwt');
const userController = require('./userController');
const { Op } = require('sequelize');

// Get Rss by Id
exports.getById = function (req, res) {
  const { rss_id } = req.params;
  if(userController.checkAccessFromReq(req,'admin',res)){
    Rss.findByPk(rss_id, {}).then((rss) => {
      res.json(rss);
    });
  }
};

// Get all Rsses
exports.getAll = async function (req, res) {
  if(userController.checkAccessFromReq(req,'admin',res)){
      Rss.findAll({
        order: [
                ['id', 'DESC'],
        ],
      }).then((Rsses) => {
        res.json(Rsses);
      });
  }
};

// Delete a Rss
exports.delete = function (req, res) {
  const { rss_id } = req.params;
  if(userController.checkAccessFromReq(req,'admin',res)){
    Rss.destroy({
      where: {
        id: rss_id,
      },
    }).then((rss) => {
      res.json({ message: 'Rss deleted' });
    });
  } else {
    res.json({ message: "you don't have the rights to do this" });
  }
};

//Update Rss
exports.update = function (req, res) {

  const rssValues = req.body;
  const { rss_id } = req.params;

  if(userController.checkAccessFromReq(req,'admin',res)){
    Rss.update(rssValues,{
      where: {
        id: rss_id,
      },
    }).then((rss) => {
      res.json({ message: 'Rss Modified' });
    });
  } else {
    res.json({ message: "you don't have the rights to do this" });
  }
};

exports.create = function (req, res) {
  console.log(req);
    if(userController.checkAccessFromReq(req,'admin',res)){
      const link  = req.body.link;
      const site = req.body.site;
      if (link == null || site == null) {
        res.status(400).json({ error: 'missing fields' });
        return;
      }
      let interval;
      if(req.body.interval== null || req.body.interval < 1 ){
        interval == 60;
      }else{
        interval == req.body.interval;
      }
      Rss.findOne({
        attributes: ['link'],
        where: {
          [Op.or]: [{ link: link }],
        },
      }).then((rssExist) => {
        if (!rssExist) {
          Rss.create({ link: link,site:site,interval:interval }).then((rss) => {
            res.status(201).json({ rss });
          });
        } else {
          res.status(409).json({ error: 'Rss already exist' });
        }
      });
    } else {
      res.json({ message: "you don't have the rights to do this" });
    }
};
