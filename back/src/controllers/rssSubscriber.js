const { Rss , Article } = require('../../database/models');

const Parser = require('rss-parser')

class RssSubscriber extends Parser {
  constructor (rss_obj ) {
    super()
    this.link = rss_obj.link
    this.id = rss_obj.id
    this.name = rss_obj.site
    this.interval = rss_obj.interval * 60000
    this.latestPublishTime = null
  }

  start () {
    console.log( 'Start Subsciber for '+ this.name +' every '+ this.interval/60000 +' mins -> '+ this.link );
    setImmediate( async () => {
        console.log('Execute RSS for '+ this.name + ' -> ' + this.link );
        let feed = await this.parseURL(this.link);
        feed.items.forEach(item => {
          Article.findOne({
            where: {
              link: item.link
            },
          }).then((article) => {
            if(!article){
              var author = item.creator;
              var title = item.title;
              var link = item.link;
              if(!author){
                author = "Unknown";
              }
              if(link && title){
                console.log('New Article from '+ this.name +' -> ' +link + ' : ' + title );
                Article.create({
                  title : title,
                  link : link,
                  author : author,
                  rssId : this.id
                });
              }
          }
        });
      });
    });

    setInterval( async () => {
        console.log('Execute RSS for '+ this.name + ' -> ' + this.link );
        let feed = await this.parseURL(this.link);
        feed.items.forEach(item => {
          Article.findOne({
            where: {
              link: item.link
            },
          }).then((article) => {
            if(!article){
              var author = item.creator;
              var title = item.title;
              var link = item.link;
              if(!author){
                author = "Unknown";
              }
              if(link && title){
                console.log('New Article from '+ this.name +' -> ' +link + ' : ' + title );
                Article.create({
                  title : title,
                  link : link,
                  author : author,
                  rssId : this.id
                });
              }
          }
        });
      });
    }, this.interval );
  }
}

async function initRssSubscribers() {
    let result = await Rss.findAll();
    result.forEach((rss_obj) => {
      var feed = new RssSubscriber( rss_obj );
      feed.start();
    });

}

initRssSubscribers();
