const { Rss, Crypto } = require('../../database/models');
const { Op } = require('sequelize');
const cryptoController = require('../controllers/cryptoController');
const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();

const Parser = require('rss-parser');

class CryptoSubscriber extends Parser {
  constructor() {
    super();
    this.name = 'Cryptos from Gecko API';
    // this.interval = 15 * 60000;
    this.interval = 15000;
  }

  start() {
    console.log(
      'Start Subsciber for ' +
        this.name +
        ' every ' +
        this.interval / 60000 +
        ' mins'
    );

    setImmediate(async () => {
      // select all crypto stored
      let cryptosStored = await Crypto.findAll({
        attributes: ['id_api'],
      });
      const cryptosStoredId = cryptosStored.map((crypto) => crypto.id_api);

      // select all from gecko where crypto is in crypto table
      // By Euro Price
      // @ts-ignore
      // const cryptoDatasGecko = await CoinGeckoClient.simple.price({
      //   vs_currencies: 'eur,usd',
      //   ids: cryptosStoredId,
      // });
      const cryptoDatasGecko = await cryptoController.getCryptosDataFromApi();
      // console.log(cryptoDatasGecko);

      for (const [currency, cryptoData] of Object.entries(cryptoDatasGecko)) {
        // for each crypto found, definition param
        for (const [key, values] of Object.entries(cryptoData)) {
          // find current crypto
          Crypto.findOne({
            where: {
              [Op.or]: [{ id_api: values.id }],
            },
          }).then((cryptoToUpdate) => {
            // replace values
            const currentPrice = 'current_price_' + currency;
            const lowestPrice = 'lowest_price_' + currency;
            const highestPrice = 'lowest_price_' + currency;

            let newValues;
            let evolution;
            if (values.price_change_percentage_24h > 0) {
              evolution = "1";
            } else if (values.price_change_percentage_24h < 0) {
              evolution = "-1";
            } else {
              evolution = "0";
            }

            if (currency == 'eur') {
              newValues = {
                current_price_eur: values.current_price,
                lowest_price_eur: values.low_24h,
                highest_price_eur: values.high_24h,
                price_change_percentage_24h: values.price_change_percentage_24h,
                url_image: values.image,
                evolution_price: evolution
              };
            } else if (currency == 'usd') {
              newValues = {
                current_price_usd: values.current_price,
                lowest_price_usd: values.low_24h,
                highest_price_usd: values.high_24h,
                price_change_percentage_24h: values.price_change_percentage_24h,
                url_image: values.image, 
                evolution_price: evolution
              };
            }

            Crypto.update(newValues, {
              where: {
                id_api: values.id,
              },
            });
          });
        }
      }
    });

    setInterval(async () => {
      // select all crypto stored
      let cryptosStored = await Crypto.findAll({
        attributes: ['id_api'],
      });
      const cryptosStoredId = cryptosStored.map((crypto) => crypto.id_api);

      // select all from gecko where crypto is in crypto table
      // By Euro Price
      // @ts-ignore
      // const cryptoDatasGecko = await CoinGeckoClient.simple.price({
      //   vs_currencies: 'eur,usd',
      //   ids: cryptosStoredId,
      // });
      const cryptoDatasGecko = await cryptoController.getCryptosDataFromApi();
      // console.log(cryptoDatasGecko);

      for (const [currency, cryptoData] of Object.entries(cryptoDatasGecko)) {
        // for each crypto found, definition param
        for (const [key, values] of Object.entries(cryptoData)) {
          // find current crypto
          Crypto.findOne({
            where: {
              [Op.or]: [{ id_api: values.id }],
            },
          }).then((cryptoToUpdate) => { 
            // replace values
            const currentPrice = 'current_price_' + currency;
            const lowestPrice = 'lowest_price_' + currency;
            const highestPrice = 'lowest_price_' + currency;

            let newValues;
            let evolution;
            if (values.price_change_percentage_24h > 0) {
              evolution = "1";
            } else if (values.price_change_percentage_24h < 0) {
              evolution = "-1";
            } else {
              evolution = "0";
            }

            if (currency == 'eur') {
              newValues = {
                current_price_eur: values.current_price,
                lowest_price_eur: values.low_24h,
                highest_price_eur: values.high_24h,
                url_image: values.image,
                price_change_percentage_24h: values.price_change_percentage_24h,
                evolution_price: evolution
              };
            } else if (currency == 'usd') {
              newValues = {
                current_price_usd: values.current_price,
                lowest_price_usd: values.low_24h,
                highest_price_usd: values.high_24h,
                price_change_percentage_24h: values.price_change_percentage_24h,
                url_image: values.image,
                evolution_price: evolution
              };
            }

            Crypto.update(newValues, {
              where: {
                id_api: values.id,
              },
            });
          });
        }
      }
    }, this.interval);
  }
}

async function initCryptoSubscribers() {
  var feed = new CryptoSubscriber();
  feed.start();
}

initCryptoSubscribers();
