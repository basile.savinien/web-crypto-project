const express = require("express");
const router = express();
const user = require("../controllers/userController");
const passport = require("passport");
const { checkJwt } = require("../utils/jwt");

/**
 * Prefix des url :
 *
 * http://localhost:3000/api/users
 */

router.post("/register", user.create);
router.post("/login", user.login);

router.get("/", checkJwt(), user.getAll);
router.get("/:user_id", checkJwt(), user.getById);
router.patch("/:user_id", checkJwt(), user.update);
router.delete("/:user_id", checkJwt(), user.delete);


module.exports = router;
