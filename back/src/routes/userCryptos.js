const express = require('express');
const router = express();
const userCrypto = require('../controllers/userCryptoController');
const passport = require('passport');
const { checkJwt } = require("../utils/jwt");

/**
 * Prefix des url :
 *
 * http://localhost:3000/api/myCryptos
 */

router.post('/:user_id/:cryptos_id', userCrypto.sub)
router.delete('/:user_id/:cryptos_id', userCrypto.unSub)
router.get('/', checkJwt() , userCrypto.getAll)

module.exports = router;
