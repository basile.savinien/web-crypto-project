const express = require('express');
const router = express();
const crypto = require('../controllers/cryptoController');
const passport = require('passport');
const { checkJwt } = require("../utils/jwt");

/**
 * Prefix des url :
 *
 * http://localhost:3000/api/cryptos
 */
router.post('/', checkJwt(), crypto.create);
router.get('/', crypto.getAll);
router.get('/active', crypto.getActive);
router.get('/full_list', crypto.getCryptoList);
router.post('/:crypto_api_id/history/:period', checkJwt(), crypto.getHistoryById);
router.get('/:crypto_id', checkJwt(), crypto.getById);
router.patch('/:crypto_id', checkJwt(), crypto.update);
router.delete('/:crypto_id', checkJwt(), crypto.delete);

module.exports = router;
