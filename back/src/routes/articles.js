const express = require('express');
const router = express();
const article = require('../controllers/articleController');
const passport = require('passport');
const { checkJwt } = require('../utils/jwt');

/**
 * Prefix des url :
 *
 * http://localhost:3000/api/articles
 */

router.get('/', article.getAll);
router.get('/tag/:article_id', article.getById);
router.get('/myArticles', checkJwt() , article.getMyArticles );


router.patch('/:article_id', article.update);
router.delete('/:article_id', article.delete);

module.exports = router;
