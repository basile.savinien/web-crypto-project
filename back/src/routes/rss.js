const express = require('express');
const router = express();
const rss = require('../controllers/rssController');
const passport = require('passport');
const { checkJwt } = require('../utils/jwt');

/**
 * Prefix des url :
 *
 * http://localhost:3000/api/articles
 */

router.get('/' , checkJwt() , rss.getAll );
router.get('/:rss_id', checkJwt() , rss.getById );

router.post('/', checkJwt(), rss.create );
router.patch('/:rss_id', checkJwt(), rss.update );
router.delete('/:rss_id', checkJwt(), rss.delete );

module.exports = router;
