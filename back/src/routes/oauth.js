const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/jwt');
// import userController from "../controllers/userController";


const userRouter = express.Router();

userRouter.get("/auth/facebook", passport.authenticate('facebook', { scope: 'email'}));

userRouter.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "/",
    failureRedirect: "/fail"
  })
);

userRouter.get("/fail", (req, res) => {
  res.send("Failed attempt");
});

userRouter.get("/", (req, res) => {
  console.log("req user", req.user)
  let optionsJwt = jwtOptions();
  let payload = { id: req.user };
  var token = jwt.sign(payload, optionsJwt.secretOrKey);
  res.redirect('http://localhost:8080/facebookSuccess?id=' + token);
});

module.exports = userRouter;